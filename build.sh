#navigate to webapi project dir
cd webapi
#restore packages
dotnet restore
#build proj
dotnet build
#publish
dotnet publish -o ./artifacts

##create docker images
###########################################

DOCKER_IMAGE_NAME="webapi"
#remove previous image
docker rmi $DOCKER_IMAGE_NAME
#build new image
docker build -t $DOCKER_IMAGE_NAME .
