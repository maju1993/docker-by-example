using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    public class CacheController : Controller
    {
        private readonly IDistributedCache _memoryCache;
 
        public CacheController(IDistributedCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        
        // GET api/cache
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                long nthPrime = FindPrimeNumber(1000); 
                var time = Encoding.UTF8.GetString(_memoryCache.Get("serverTime"));
                ViewBag.data = time;
 
                return Ok($"Server time: {time} - Machine name: {Environment.MachineName}");
            }
            catch (Exception ex)
            {
                return Ok(ex.GetBaseException().Message);
            }
        }

        public long FindPrimeNumber(int n)
        {
            int count=0;
            long a = 2;
            while(count<n)
            {
                long b = 2;
                int prime = 1;// to check if found a prime
                while(b * b <= a)
                {
                    if(a % b == 0)
                    {
                        prime = 0;
                        break;
                    }
                    b++;
                }
                if(prime > 0)
                {
                    count++;
                }
                a++;
            }
            return (--a);
        }


        // get api/cache/post
        [HttpGet("post")]
        public IActionResult Post()
        {
            try
            {
                var time = DateTime.Now.ToLocalTime().ToString(CultureInfo.InvariantCulture);
                var cacheOptions = new DistributedCacheEntryOptions
                {
                    AbsoluteExpiration = DateTime.Now.AddYears(1)
                };
                _memoryCache.Set("serverTime", Encoding.UTF8.GetBytes(time), cacheOptions);
 
                return Ok(new { status = true });
            }
            catch (Exception ex)
            {
                return Json(new { ex = ex });
            }
        }

        // DELETE api/cache
        [HttpGet("delete")]
        public IActionResult Delete()
        {
            _memoryCache.Remove("serverTime");
 
            return Ok(true);
        }
    }
}
