docker rm -f $(docker ps -aq)
docker-compose up -d redis
sleep 5
docker-compose up -d webapi
docker-compose scale webapi=3
docker-compose up -d load_balancer